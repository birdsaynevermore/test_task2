<?php
	session_start();
	error_reporting(E_ALL);
	if(isset($_SESSION['admin'])){
		if($_SESSION['admin'] == "admin"){
			header("Location: index.php");
		}
	}
	require_once 'head.php';
	if(!empty($_POST['submit'])){
		login();
	}
?>  
<br />
<form method="post">
	<div class="form-group">
		<label for="user">Логин</label>
		<input type="text" name="user" class="form-control" id="user" placeholder="Логин" required />
	</div>
	<div class="form-group">
		<label for="pass">Пароль</label>
		<input type="password" name="pass" class="form-control" id="pass" placeholder="Пароль" required />
	</div>
	<button type="submit" class="btn btn-primary" name="submit" value="Войти">Войти</button>
</form>
<?php
	require_once 'footer.php';