<?php
	require_once 'header.php';
	if(!empty($_POST['movie'])){
		addMovie();
	}
	if(!empty($_POST['deleteMov'])){
		deleteMovie();
	}
	if(!empty($_POST['movieEdit'])){
		movieEdit();
	}
	
	if(!empty($_GET['page'])){
		$movies = printMovies($_GET['page']);
	} else {
		$movies = printMovies(0);
	}
	$directors = printDirectors();
	
?>
	<div class="row text-center onlyMobile"><i class="fas fa-arrows-alt-h"></i></div>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Фильм</th>
					<th scope="col">Режиссёр</th>
					<th scope="col">Описание</th>
					<th scope="col">Дата релиза</th>
					<th scope="col" class="text-center">Редактировать</th>
					<th scope="col" class="text-center">Удалить</th>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($movies)) { ?>
				<?php foreach($movies as $movie) { ?>
				<tr>
					<td><?=$movie['name']?></td>
					<td><?php  printDirector($movie['directorId']); ?></td>
					<td><?=$movie['description']?></td>
					<td><?=$movie['releaseDate']?></td>
					<td class="text-center editButton">
						<button type="submit" name="editMov" data-toggle="modal" data-target="#editMovieModal" onclick="editFunction(<?=$movie['movieId']?>, `<?=$movie['name'] ?>`, <?=$movie['directorId']?>, `<?=$movie['releaseDate']?>`<?php if($movie['description']==!'') { echo ', `' . $movie['description'] . '`';}?>);"><i class="fas fa-pen"></i></button>
					</td>
					<td class="text-center deleteButton">
						<form method="post">
							<input type="hidden" name="movieDelId" value="<?=$movie['movieId']?>" />
							<button type="submit" name="deleteMov" value="delete" onclick="return confirm(`Вы уверены, что хотите удалить фильм «<?=$movie['name']?>»?`)"><i class="fas fa-times"></i></button>
						</form>
					</td>
				</tr>
				<?php } ?>
			<?php } ?>
			</tbody>
		</table>
	</div>
<nav aria-label="Page navigation example">
<ul class="pagination justify-content-center">
<?php
	if(!empty($_GET['page'])){
		$movies = printPagination($_GET['page'], 'movie');
	} else {
		$movies = printPagination(1, 'movie');
	}
 ?>
</ul>
</nav>
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addMovieModal">Добавить фильм</button>
	<script>
		$(document).ready(function () {
			$(".pageLinks").removeClass("active");
			$("#moviesLink").addClass("active");
		});
		function editFunction(movieId, name, directorId, releaseDate, description) {
			$("#movieIdEdit").val(movieId);
			$("#movieNameEdit").val(name);
			$("#releaseDateEdit").val(releaseDate);
			$("#directorIdEdit").val(directorId);
			$("#movieDescriptionEdit").val(description);
			
		}
	</script>
<div class="modal fade" id="addMovieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Добавить фильм</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form method="post">
<div class="modal-body">

	<div class="form-group">
		<label for="directorId">Режиссёр</label>
		<select name="directorId" id="directorId" class="form-control" required>
			<?php if(!empty($directors)) { ?>
				<?php foreach($directors as $director) { ?>
					<option value="<?=$director['directorId']?>"><?=$director['name']?></option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="movieName">Название</label>
		<input type="text" name="name" class="form-control" id="movieName" placeholder="Название" required />
	</div>
	<div class="form-group">
		<label for="releaseDate">Дата релиза</label>
		<input type="date" id="releaseDate" name="releaseDate" class="form-control" required />
	</div>
	<div class="form-group">
		<label for="movieDescription">Описание</label>
		<textarea name="description" id="movieDescription" placeholder="Описание" class="form-control"></textarea>
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
<button type="submit" class="btn btn-primary" name="movie" value="Добавить">Добавить</button>
</div>
</form>
</div>
</div>
</div>



<div class="modal fade" id="editMovieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Редактировать фильм</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form method="post">
<div class="modal-body">
	<input type="hidden" name="movieId" id="movieIdEdit" value="" />
	<div class="form-group">
		<label for="directorIdEdit">Режиссёр</label>
		<select name="directorId" id="directorIdEdit" class="form-control" required>
			<?php if(!empty($directors)) { ?>
				<?php foreach($directors as $director) { ?>
					<option value="<?=$director['directorId']?>"><?=$director['name']?></option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="movieNameEdit">Название</label>
		<input type="text" name="name" class="form-control" id="movieNameEdit" placeholder="Название" required />
	</div>
	<div class="form-group">
		<label for="releaseDateEdit">Дата релиза</label>
		<input type="date" id="releaseDateEdit" name="releaseDate" class="form-control" required />
	</div>
	<div class="form-group">
		<label for="movieDescriptionEdit">Описание</label>
		<textarea name="description" id="movieDescriptionEdit" placeholder="Описание" class="form-control"></textarea>
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
<button type="submit" class="btn btn-primary" name="movieEdit" value="Сохранить">Сохранить</button>
</div>
</form>
</div>
</div>
</div>


<?php
	require_once 'footer.php';