<?php
	require_once 'header.php';
	if(!empty($_POST['directorAdd'])){
		addDiirector();
	}
	if(!empty($_POST['deleteDirector'])){
		deleteDirector();
	}
	if(!empty($_POST['directorEdit'])){
		directorEdit();
	}
	if(!empty($_GET['page'])){
		$directors = printDirectorsList($_GET['page']);
	} else {
		$directors = printDirectorsList(0);
	}
?>


<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th class="col">Режиссёр</th>
				<th class="col col-lg-2 text-center">Редактировать</th>
				<th class="col col-lg-2 text-center">Удалить</th>
			</tr>
		</thead>
		<tbody>
		<?php if(!empty($directors)) { ?>
			<?php foreach($directors as $director) { ?>
			<tr>
				<td><?=$director['name']?></td>
				<td class="col col-lg-2 text-center editButton">
					<button type="submit" name="editMov" data-toggle="modal" data-target="#editDirectorModal" onclick="editFunction(<?=$director['directorId']?>, `<?=$director['name']?>`);"><i class="fas fa-user-edit"></i></button>
				</td>
				<td class="col col-lg-2 text-center deleteButton">
					<form method="post">
						<input type="hidden" name="directorDelId" value="<?=$director['directorId']?>" />
						<button type="submit" name="deleteDirector" value="delete" onclick="return confirm(`Вы уверены, что хотите удалить режиссёра «<?=$director['name']?>»?`)"><i class="fas fa-user-times"></i></button>
					</form>
				</td>
			</tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
</div>
<nav aria-label="Page navigation example">
<ul class="pagination justify-content-center">
<?php
	if(!empty($_GET['page'])){
		$movies = printPagination($_GET['page'], 'director');
	} else {
		$movies = printPagination(1, 'director');
	}
 ?>
</ul>
</nav>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addDirectorModal">Добавить режиссёра</button>
	<script>
		$(document).ready(function () {
			$(".pageLinks").removeClass("active");
			$("#directorsLink").addClass("active");
		});
		function editFunction(directorId, name) {
			$("#directorIdEdit").val(directorId);
			$("#directorNameEdit").val(name);			
		}
	</script>
<div class="modal fade" id="addDirectorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Добавить режиссёра</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form method="post">
<div class="modal-body">
	<div class="form-group">
		<label for="directorName">Режиссёр</label>
		<input type="text" name="name" class="form-control" id="directorName" placeholder="Имя режиссёра" minlength="3" maxlength="255" pattern="^[A-Za-zА-Яа-яЁёіІїЇ\s]+$" required />
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
<button type="submit" class="btn btn-primary" name="directorAdd" value="Добавить">Добавить</button>
</div>
</form>
</div>
</div>
</div>

<div class="modal fade" id="editDirectorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Редактировать режиссёра</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form method="post">
<div class="modal-body">
	<input type="hidden" name="directorId" id="directorIdEdit" value="" />
	<div class="form-group">
		<label for="directorNameEdit">Режиссёр</label>
		<input type="text" name="name" class="form-control" id="directorNameEdit" placeholder="Имя режиссёра" minlength="3" maxlength="255" pattern="^[A-Za-zА-Яа-яЁёіІїЇ\s]+$" required />
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
<button type="submit" class="btn btn-primary" name="directorEdit" value="Сохранить">Сохранить</button>
</div>
</form>
</div>
</div>
</div>
<?php
	require_once 'footer.php';