<?php
	session_start();
	error_reporting(E_ALL);
	if(isset($_GET['do'])){
		if($_GET['do'] == 'logout'){
			unset($_SESSION['admin']);
			session_destroy();
			header("Location: login.php");
			exit; 
		}
	}
	if(isset($_SESSION['admin'])){
		if($_SESSION['admin'] != "admin"){
			header("Location: login.php");
			exit; 
		}
	} else {
		header("Location: login.php");
		exit;
	}
	//header("Content-type: text/html; charset=utf-8");
	require_once 'head.php';
	?>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="/">Логотип</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item pageLinks" id="moviesLink">
							<a class="nav-link" href="/">Фильмы</a>
						</li>
						<li class="nav-item pageLinks" id="directorsLink">
							<a class="nav-link" href="directors.php">Режисёры</a>
						</li>
					</ul>
					<ul class="navbar-nav my-2 my-lg-0">
						<li class="nav-item active">
							<a class="nav-link" href="index.php?do=logout">Выход</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<div class="container">