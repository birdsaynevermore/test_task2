<?php
	$printLimit = 2;
	function clear(){
		global $database;
		foreach ($_POST as $key => $value) {
			$_POST[$key] = htmlspecialchars($value);
		}
	}
	function login() {
		global $database;
		extract($_POST);
		$query = "SELECT * FROM users WHERE name = '$user'" ;
		$resource = mysqli_query($database, $query) or die("Ошибка " . mysqli_error($database));
		$row = mysqli_fetch_array($resource);
		if($row['name'] == $user AND password_verify($pass, $row['password'])) {
			if($row['is_admin'] == true) {
				$_SESSION['admin'] = $row['name'];
				echo '<script>window.location.href = "index.php";</script>';
			} else {
				echo '<div class="alert alert-warning" role="alert">У вас недостаточно прав!</div>';
			}
		} else {
			echo '<div class="alert alert-danger" role="alert">Логин или пароль неверны!</div>';
		}
	}
	function addMovie() {
		global $database;
		clear();
		extract($_POST);
		$queryFind = "SELECT * FROM movie WHERE directorId = '$directorId' AND name = '$name'";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
			if ($row_cnt == 0) {
				$query = "INSERT INTO movie (directorId, name, description, releaseDate) VALUES ('$directorId', '$name', '$description', '$releaseDate')";
				if($name !== "" && $releaseDate !== "" && $directorId !== "") {
					mysqli_query($database, $query);
					echo '<div class="alert alert-success" role="alert">Фильм добавлен!</div>';
				} else {
					echo '<div class="alert alert-danger" role="alert">Заполните все поля!</div>';
				}
			} else {
				echo '<div class="alert alert-warning" role="alert">Такой фильм уже существует!</div>';
			}
		}
	}
	function addDiirector() {
		global $database;
		clear();
		extract($_POST);
		$queryFind = "SELECT * FROM director WHERE name = '$name'";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
			if ($row_cnt == 0) {
				$query = "INSERT INTO director (name) VALUES ('$name')";
				if($name !== "") {
					mysqli_query($database, $query);
					echo '<div class="alert alert-success" role="alert">Режиcсёр добавлен!</div>';
				} else {
					echo '<div class="alert alert-danger" role="alert">Заполните поле имени!</div>';
				}
			} else {
				echo '<div class="alert alert-warning" role="alert">Такой режиcсёр уже существует!</div>';
			}
		}
	}
	function printDirectors() {
		global $database;
		$query = "SELECT * FROM director";
		$result = mysqli_query($database, $query);
		return mysqli_fetch_all($result, MYSQLI_ASSOC);
	}
	function printDirectorsList($page) {
		global $database;
		global $printLimit;
		if($page!==0) {
			$page = $printLimit*($page-1);
		}
		$query = "SELECT * FROM director LIMIT $page, $printLimit";
		$result = mysqli_query($database, $query);
		return mysqli_fetch_all($result, MYSQLI_ASSOC);
	}
	function printDirector($id) {
		global $database;
		$query = "SELECT name FROM director WHERE directorId = '$id'";
		$result = mysqli_query($database, $query);
		$directorName = mysqli_fetch_all($result, MYSQLI_ASSOC);
		echo $directorName[0]['name'];
	}
	function printMovies($page) {
		global $database;
		global $printLimit;
		if($page!==0) {
			$page = $printLimit*($page-1);
		}
		$query = "SELECT * FROM movie LIMIT $page, $printLimit";
		$result = mysqli_query($database, $query);
		return mysqli_fetch_all($result, MYSQLI_ASSOC);
	}
	function printPagination($page, $listType) {
		global $database;
		global $printLimit;
			$queryFind = "SELECT * FROM $listType";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
		}
		$page_count = ceil($row_cnt / $printLimit);
		$i = 0;
		if ($page_count > 1) {
			while($i < $page_count) {
				$i++;
				if ($page == $i) {
					echo "<li class='page-item active'><a class='page-link' href='?page=$i'>$i</a></li>";
				} else {
					echo "<li class='page-item'><a class='page-link' href='?page=$i'>$i</a></li>";
				}
			}
		}
	}
	function printPaginationDirectors() {
		global $database;
		global $printLimit;
		$queryFind = "SELECT * FROM director";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
		}
		$page_count = ceil($row_cnt / $printLimit);
		$i = 1;
		if ($page_count > 1) {
			echo "<li class='page-item' id='page$i'><a class='page-link' href='/'>$i</a></li>";
			while($i < $page_count) {
				$i++;
				echo "<li class='page-item' id='page$i'><a class='page-link' href='?page=$i'>$i</a></li>";
			}
		}
	}
	function deleteMovie() {
		global $database;
		extract($_POST);
		$query = "DELETE FROM movie WHERE movieId = '$movieDelId'";
		mysqli_query($database, $query);
		echo '<div class="alert alert-danger" role="alert">Фильм удалён!</div>';
	}
	function movieEdit() {
		global $database;
		clear();
		extract($_POST);
		$queryFind = "SELECT * FROM movie WHERE directorId = '$directorId' AND name = '$name'";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
			if ($row_cnt == 0) {
				$query = "UPDATE movie SET directorId = '$directorId', name = '$name', description = '$description', releaseDate = '$releaseDate' WHERE movieId = '$movieId'";
				if($name !== "" && $releaseDate !== "" && $directorId !== "") {
					mysqli_query($database, $query);
					echo '<div class="alert alert-success" role="alert">Фильм отредактирован!</div>';
				} else {
					echo '<div class="alert alert-danger" role="alert">Заполните все поля!</div>';
				}
			} else {
				$queryFind = "SELECT * FROM movie WHERE directorId = '$directorId' AND name = '$name' AND movieId = '$movieId'";
				if ($result = $database->query($queryFind)) {
					$row_cnt = $result->num_rows;
					if ($row_cnt == 1) {
						$query = "UPDATE movie SET directorId = '$directorId', name = '$name', description = '$description', releaseDate = '$releaseDate' WHERE movieId = '$movieId'";
						if($name !== "" && $releaseDate !== "" && $directorId !== "") {
							mysqli_query($database, $query);
							echo '<div class="alert alert-success" role="alert">Фильм отредактирован!</div>';
						} else {
							echo '<div class="alert alert-danger" role="alert">Заполните все поля!</div>';
						}
					} else {
						echo '<div class="alert alert-warning" role="alert">Такой фильм уже существует!</div>';
					}
				}
			}
		}
	}
	function deleteDirector() {
		global $database;
		extract($_POST);
		$query = "DELETE FROM director WHERE directorId = '$directorDelId'";
		mysqli_query($database, $query);
		$query = "DELETE FROM movie WHERE directorId = '$directorDelId'";
		mysqli_query($database, $query);
	}
	function directorEdit() {
		global $database;
		clear();
		extract($_POST);
		$queryFind = "SELECT * FROM director WHERE name = '$name'";
		if ($result = $database->query($queryFind)) {
			$row_cnt = $result->num_rows;
			if ($row_cnt == 0) {
				$query = "UPDATE director SET name = '$name' WHERE directorId = '$directorId'";
				if($name !== "") {
					mysqli_query($database, $query);
					echo '<div class="alert alert-success" role="alert">Режиссёр отредактирован!</div>';
				} else {
					echo '<div class="alert alert-danger" role="alert">Заполните поле имени!</div>';
				}
			} else {
				$queryFind = "SELECT * FROM director WHERE name = '$name' AND directorId = '$directorId'";
				if ($result = $database->query($queryFind)) {
					$row_cnt = $result->num_rows;
					if ($row_cnt == 1) {
						$query = "UPDATE director SET name = '$name' WHERE directorId = '$directorId'";
						if($name !== "") {
							mysqli_query($database, $query);
							echo '<div class="alert alert-success" role="alert">Режиссёр отредактирован!</div>';
						} else {
							echo '<div class="alert alert-danger" role="alert">Заполните поле имени!</div>';
						}
					} else {
						echo '<div class="alert alert-warning" role="alert">Такой режиссёр уже существует!</div>';
					}
				}
			}
		}
	}