<?php require_once 'connect.php';
require_once 'functions.php';
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="fontawesome/css/all.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>
<div class="container">